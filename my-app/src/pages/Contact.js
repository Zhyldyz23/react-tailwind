import React from 'react';

const Contact = () => {
  return (
    <div>
      <h1 className="text-3xl font-bold">Contact</h1>
      <p>Welcome to the Contact page!</p>
    </div>
  );
}

export default Contact;
