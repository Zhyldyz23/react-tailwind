import React from 'react';

const Home = () => {
  return (
    <div>
      <h1 className="text-3xl font-bold">Home</h1>
      <p className="first-line:uppercase first-line:tracking-widest first-letter:text-7xl first-letter:font-bold first-letter:text-slate-900 first-letter:mr-3 first-letter:float-left">
        Well, let me tell you something, funny boy. Y'know that little stamp, the one
        that says "New York Public Library"? Well that may not mean anything to you,
        but that means a lot to me. One whole hell of a lot.
      </p>
    </div>
  );
}

export default Home;
