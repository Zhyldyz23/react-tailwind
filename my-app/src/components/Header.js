import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => {
  return (
    <header className="bg-gray-800 text-white p-4">
      <nav className="flex space-x-4">
        <NavLink 
          to="/" 
          exact="true"
          className={({ isActive }) => 
            isActive ? 'text-yellow-500' : 'hover:text-yellow-500'
          }
        >
          Home
        </NavLink>
        <NavLink 
          to="/about" 
          className={({ isActive }) => 
            isActive ? 'text-yellow-500' : 'hover:text-yellow-500'
          }
        >
          About
        </NavLink>
        <NavLink 
          to="/contact" 
          className={({ isActive }) => 
            isActive ? 'text-yellow-500' : 'hover:text-yellow-500'
          }
        >
          Contact
        </NavLink>
        <NavLink 
          to="/login" 
          className={({ isActive }) => 
            isActive ? 'text-yellow-500' : 'hover:text-yellow-500'
          }
        >
          Login
        </NavLink>
      </nav>
    </header>
  );
}

export default Header;
