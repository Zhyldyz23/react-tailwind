import React from 'react';
import ReactDOM from 'react-dom';
import './index.css'; // Tailwind CSS sollte hier importiert werden
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
